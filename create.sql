
    drop table if exists public.Actor cascade;

    drop table if exists public.AehnlichesProdukt cascade;

    drop table if exists public.Autor cascade;

    drop table if exists public.BeteiligtAn cascade;

    drop table if exists public.Bewertung cascade;

    drop table if exists public.BietetAn cascade;

    drop table if exists public.Buch cascade;

    drop table if exists public.Creator cascade;

    drop table if exists public.Director cascade;

    drop table if exists public.DVD cascade;

    drop table if exists public.Filiale cascade;

    drop table if exists public.GehoertZuKategorie cascade;

    drop table if exists public.kategorie cascade;

    drop table if exists public.Kauf cascade;

    drop table if exists public.Kuenstler cascade;

    drop table if exists public.Kunde cascade;

    drop table if exists public.Label cascade;

    drop table if exists public.MusikCD cascade;

    drop table if exists public.Produkt cascade;

    drop table if exists public.Titel cascade;

    drop table if exists public.Unterkategorie cascade;

    create table public.Actor (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.AehnlichesProdukt (
       produktNummer2 varchar(255) not null,
        produktNummer1 varchar(255) not null,
        primary key (produktNummer2, produktNummer1)
    );

    create table public.Autor (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.BeteiligtAn (
       produktNummer varchar(255) not null,
        name varchar(255) not null,
        beteiligungsart varchar(255) not null,
        primary key (produktNummer, name, beteiligungsart)
    );

    create table public.Bewertung (
       produktNummer varchar(255) not null,
        kontonummer varchar(255) not null,
        punkte int4 not null,
        rezension varchar(1000) not null,
        primary key (produktNummer, kontonummer)
    );

    create table public.BietetAn (
       produktNummer varchar(255) not null,
        nameAnschrift varchar(255) not null,
        preis float4,
        verfuegbarkeit boolean not null,
        zustand varchar(255) not null,
        primary key (produktNummer, nameAnschrift)
    );

    create table public.Buch (
       produktNummer varchar(255) not null,
        ISBN varchar(255) not null,
        Verlag varchar(255) not null,
        erscheinungsDatum varchar(255) not null,
        seitenZahl int4 not null,
        primary key (produktNummer)
    );

    create table public.Creator (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.Director (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.DVD (
       produktNummer varchar(255) not null,
        format varchar(255) not null,
        laufZeit int4 not null,
        regionCode varchar(255) not null,
        primary key (produktNummer)
    );

    create table public.Filiale (
       name varchar(255) not null,
        anschrift varchar(255) not null,
        primary key (name, anschrift)
    );

    create table public.GehoertZuKategorie (
       produktNummer varchar(255) not null,
        kategorieID varchar(255) not null,
        primary key (produktNummer, kategorieID)
    );

    create table public.kategorie (
       kategorie_id varchar(255) not null,
        name varchar(255) not null,
        ueber_kategorie_id varchar(255),
        primary key (kategorie_id)
    );

    create table public.Kauf (
       produktNummer varchar(255) not null,
        kontonummer varchar(255) not null,
        kaufzeitpunkt varchar(255) not null,
        primary key (produktNummer, kontonummer, kaufzeitpunkt)
    );

    create table public.Kuenstler (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.Kunde (
       kontonummer varchar(255) not null,
        lieferAdresse varchar(255) not null,
        primary key (kontonummer)
    );

    create table public.Label (
       name varchar(255) not null,
        primary key (name)
    );

    create table public.MusikCD (
       produktNummer varchar(255) not null,
        erscheinungsDatum varchar(255) not null,
        primary key (produktNummer)
    );

    create table public.Produkt (
       produktNummer varchar(255) not null,
        bild varchar(255),
        rating float4 not null,
        titel varchar(255) not null,
        verkaufsRang int4 not null,
        primary key (produktNummer)
    );

    create table public.Titel (
       produktNummer varchar(255) not null,
        nummer int4 not null,
        name varchar(255) not null,
        primary key (produktNummer, nummer, name)
    );

    create table public.Unterkategorie (
       UKategorieID varchar(255) not null,
        OKategorieID varchar(255) not null,
        primary key (UKategorieID, OKategorieID)
    );
