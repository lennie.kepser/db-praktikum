BEGIN;

CREATE OR REPLACE FUNCTION insertUpdate()
    RETURNS TRIGGER AS
$$
BEGIN
    UPDATE produkt SET rating = (
        SELECT AVG(punkte) FROM public.bewertung
        WHERE produkt_produktnummer = coalesce(NEW.produkt_produktnummer, OLD.produkt_produktnummer)
    )
    WHERE produktnummer = coalesce(NEW.produkt_produktnummer, OLD.produkt_produktnummer);
    RETURN null;
END;
$$
    LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS insert_update on public.bewertung;
-- Delete Trigger auch mit drin, fuer den Fall, dass eine Bewertung gelöscht wird.
CREATE TRIGGER insert_update
    AFTER INSERT OR UPDATE OR DELETE
    ON public.bewertung
    FOR EACH ROW
EXECUTE PROCEDURE insertUpdate();

DELETE FROM bewertung WHERE bewertungsid = 7000;

INSERT INTO public.bewertung
(bewertungsid, punkte, rezension, username, produkt_produktnummer, kunde_kontonummer)
VALUES
    (7000, 1, 'Dieses Buch ist sehr schoen', 'WolfiSchäuble42', 'B00069KW58', NULL);

INSERT INTO public.bewertung
(punkte, rezension, username, produkt_produktnummer, kunde_kontonummer)
VALUES
    (3, 'Dieses Buch ist sehr schoen', 'WolfiSchäu', 'B00069KW58', NULL);

COMMIT;
