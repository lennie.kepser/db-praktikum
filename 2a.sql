-- 1
-- Wieviele Produkte jeden Typs (Buch, Musik-CD, DVD) sind in der Datenbank erfasst? Hinweis: Geben Sie das Ergebnis in einer 3-spaltigen Relation aus.

SELECT(SELECT COUNT(*)
       FROM public.Buch) as Total_Buecher,
    (
    SELECT COUNT(*)
    FROM  public.DVD) as Total_DVDs,
    (SELECT COUNT(*)
    FROM public.MusikCD) as Total_MusikCDs;

-- 2 Nennen Sie die 5 besten Produkte jedes Typs (Buch, Musik-CD, DVD) sortiert nach dem durchschnittlichem Rating. Hinweis: Geben Sie das Ergebnis in einer einzigen Relation mit den Attributen Typ, ProduktNr, Rating aus. Wie werden gleiche durchschnittliche Ratings behandelt?

SELECT 'Buch' AS Typ, produktnummer, rating FROM public.buch
    JOIN public.produkt ON public.buch.produkt_produktnummer = public.produkt.produktnummer
    ORDER BY rating
    DESC, amnt_ratings DESC, verkaufsrang DESC, produktnummer ASC LIMIT 5;

SELECT 'DVD' AS Typ, produktnummer, rating FROM public.dvd
                                                     JOIN public.produkt ON public.dvd.produkt_produktnummer = public.produkt.produktnummer
ORDER BY rating
        DESC, amnt_ratings DESC, verkaufsrang DESC, produktnummer ASC LIMIT 5;

SELECT 'MusikCD' AS Typ, produktnummer, rating FROM public.musikcd
                                                     JOIN public.produkt ON public.musikcd.produkt_produktnummer = public.produkt.produktnummer
ORDER BY rating
        DESC, amnt_ratings DESC, verkaufsrang DESC, produktnummer ASC LIMIT 5;

-- 3 Für welche Produkte gibt es im Moment kein Angebot?

SELECT * FROM public.produkt
         WHERE produktnummer NOT IN (SELECT public.bietetan.produktnummer_produktnummer FROM public.bietetan WHERE verfuegbarkeit = True);

-- 4 Für welche Produkte ist das teuerste Angebot mehr als doppelt so teuer wie das preiswerteste?



SELECT * FROM public.produkt
    WHERE (
        SELECT preis FROM public.bietetan WHERE produktnummer_produktnummer = produkt.produktnummer ORDER BY preis DESC LIMIT 1
              ) >
        (SELECT preis * 2 FROM public.bietetan WHERE produktnummer_produktnummer = produkt.produktnummer ORDER BY preis ASC LIMIT 1);


-- 5 Welche Produkte haben sowohl mindestens eine sehr schlechte (Punktzahl: 1) als auch mindestens eine sehr gute (Punktzahl: 5) Bewertung?

SELECT DISTINCT produkt_produktnummer FROM ((SELECT * FROM public.bewertung
    WHERE punkte = 1 OR punkte =  5 ) as "B"
JOIN public.produkt ON produkt_produktnummer = produkt.produktnummer);

-- 6 Für wieviele Produkte gibt es gar keine Rezension?

SELECT COUNT(*) FROM public.produkt
    WHERE produktnummer NOT IN (SELECT produkt_produktnummer FROM public.bewertung);

-- 7 Nennen Sie alle Rezensenten, die mindestens 10 Rezensionen geschrieben haben.

SELECT username, COUNT(*) AS "anzahl_bewertungen" FROM public.bewertung
GROUP BY username
HAVING COUNT(*) > 10;

-- 8 Geben Sie eine duplikatfreie und alphabetisch sortierte Liste der Namen aller Buchautoren an, die auch an DVDs oder Musik-CDs beteiligt sind.



SELECT * FROM beteiligter
WHERE beteiligter.name IN (SELECT beteiligtan.name FROM beteiligtan JOIN buch ON produktnummer_produktnummer = produkt_produktnummer)
    AND (beteiligter.name IN (SELECT beteiligtan.name FROM beteiligtan JOIN dvd ON produkt_produktnummer = produktnummer_produktnummer)
    OR beteiligter.name IN (SELECT beteiligtan.name FROM beteiligtan JOIN musikcd ON produkt_produktnummer = produktnummer_produktnummer));


-- 9 Wie hoch ist die durchschnittliche Anzahl von Liedern einer Musik-CD?

SELECT AVG("C") AS average_titel FROM (SELECT COUNT(*) AS "C" FROM titel
GROUP BY produkt_produktnummer ) as tC;

-- 10 Für welche Produkte gibt es ähnliche Produkte in einer anderen Hauptkategorie? Hinweis: Eine Hauptkategorie ist eine Produktkategorie ohne Oberkategorie. Erstellen Sie eine rekursive Anfrage, die zu jedem Produkt dessen Hauptkategorie bestimmt.
SELECT DISTINCT produkt1_produktnummer FROM aehnlichesprodukt
         WHERE EXISTS(

        SELECT * FROM (
            WITH RECURSIVE hauptkategorie AS (SELECT ueberkategorie_kategorieid AS u, kategorieid FROM kategorie WHERE kategorieid IN (SELECT kategorie_kategorieid FROM gehoertzukategorie WHERE produkt1_produktnummer = produkt_produktnummer)
                UNION ALL
            SELECT ueberkategorie_kategorieid,kategorie.kategorieid FROM hauptkategorie, kategorie WHERE hauptkategorie.u = kategorie.kategorieid)
            SELECT kategorieid AS Hauptkategorie FROM hauptkategorie WHERE u IS NULL)
            as produkt1Haupt
        EXCEPT
        SELECT * FROM (
            WITH RECURSIVE hauptkategorie AS (SELECT ueberkategorie_kategorieid AS u, kategorieid FROM kategorie WHERE kategorieid IN (SELECT kategorie_kategorieid FROM gehoertzukategorie WHERE produkt2_produktnummer = produkt_produktnummer)
                UNION ALL
            SELECT ueberkategorie_kategorieid,kategorie.kategorieid FROM hauptkategorie, kategorie WHERE hauptkategorie.u = kategorie.kategorieid)
            SELECT kategorieid AS Hauptkategorie FROM hauptkategorie WHERE u IS NULL)
            as produkt2Haupt);

--  11 Welche Produkte werden in allen Filialen angeboten? Hinweis: Ihre Query muss so formuliert werden, dass sie für eine beliebige Anzahl von Filialen funktioniert. Hinweis: Beachten Sie, dass ein Produkt mehrfach von einer Filiale angeboten werden kann (z.B. neu und gebraucht).



SELECT * FROM produkt
WHERE NOT EXISTS(
    SELECT DISTINCT name FROM filiale
    EXCEPT
    SELECT name_name as name FROM bietetan WHERE produktnummer_produktnummer = produkt.produktnummer
    );

-- 12 In wieviel Prozent der Fälle der Frage 11 gibt es in Leipzig das preiswerteste Angebot?


SELECT MinPriceInLeipzig.C as CountMinPriceInLeipzig ,Total.C as Total, MinPriceInLeipzig.C / Total.C * 100 as Percentage FROM (SELECT count(*)::float as C FROM produkt
JOIN bietetan ON produkt.produktnummer = bietetan.produktnummer_produktnummer
WHERE NOT EXISTS(
        SELECT DISTINCT name FROM filiale
        EXCEPT
        SELECT name_name as name FROM bietetan WHERE produktnummer_produktnummer = produkt.produktnummer
    )
    AND bietetan.name_name = 'Leipzig'
    AND bietetan.preis = (
        SELECT MIN(preis) FROM bietetan
        WHERE produktnummer_produktnummer = produkt.produktnummer
    )) as MinPriceInLeipzig,
    (SELECT COUNT(*)::float as C FROM produkt
WHERE NOT EXISTS(
    SELECT DISTINCT name FROM filiale
    EXCEPT
    SELECT name_name as name FROM bietetan WHERE produktnummer_produktnummer = produkt.produktnummer
    )) as Total
