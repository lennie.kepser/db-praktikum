
package com.example.demo;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class BietetAn implements Serializable {


    @Id
    @ManyToOne
    @JoinColumn(nullable = false)
    private Filiale name;
    @Id
    @ManyToOne
    @JoinColumn(nullable = false)
    private Produkt produktNummer;
    @Column(nullable = true)
    @Range(min = 0, message = "Preis muss Positiv sein")
    private float preis;
    @Column(nullable = false)
    private boolean verfuegbarkeit;
    @Column(nullable = false)
    private String zustand;


    public void setPreis(float preis) {
        this.preis = preis;
    }

    public void setVerfuegbarkeit(boolean verfuegbarkeit) {
        this.verfuegbarkeit = verfuegbarkeit;
    }

    public String getZustand() {
        return zustand;
    }

    public void setZustand(String zustand) {
        this.zustand = zustand;
    }

    public boolean getVerfuegbarkeit() {
        return verfuegbarkeit;
    }

    public float getPreis() {
        return preis;
    }


    public Produkt getProduktNummer() {
        return produktNummer;
    }

    public void setProduktNummer(Produkt produktNummer) {
        this.produktNummer = produktNummer;
    }

    public boolean isVerfuegbarkeit() {
        return verfuegbarkeit;
    }

    public Filiale getName() {
        return name;
    }

    public void setName(Filiale name) {
        this.name = name;
    }
}
