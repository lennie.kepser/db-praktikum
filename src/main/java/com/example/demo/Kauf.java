package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class Kauf implements Serializable {
    @Id
    @Column(nullable = false)
    private String kaufzeitpunkt;

    @Id
    private String kontonummer;

    @OneToOne
    @MapsId("kontonummer")
    private Kunde kunde;

    @Id
    private String produktNummer;

    @ManyToOne
    @MapsId("produktNummer")
    private Produkt produkt;

    public String getKaufzeitpunkt() {
        return kaufzeitpunkt;
    }

    public void setKaufzeitpunkt(String kaufzeitpunkt) {
        this.kaufzeitpunkt = kaufzeitpunkt;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kontoNummer) {
        this.kunde = kontoNummer;
    }
}
