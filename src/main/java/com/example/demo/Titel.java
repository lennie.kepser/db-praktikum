package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class Titel implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn
    private Produkt produkt;

    @Id
    private String name;
    @Id
    private int nummer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }
}
