package com.example.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.PersistenceException;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReviewLoader {

    private String getColumnRowWithoutQuotation(int row, int column, List<List<String>> reviews){
        // entfernen von " " vor und hinter der Daten -> Fehler bei dem Benutzen von .isEmpty() zu vermeiden
        return reviews.get(row).get(column).replace("\"", "");
    }
    private static final String COMMA_DELIMITER = ","; // "," als Abtrennungsmerkmal
    double addToAverage(double average, int size, double value)
    {
        return (size * average + value) / (size + 1);
    }
    public boolean loadReview(String file, SessionFactory sessionFactory) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        StringBuilder errorList = new StringBuilder();

        int nullFehlerCount = 0;
        int konsistenzErrorCount = 0;
        List<List<String>> reviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) { //einlesen der CSV Datei in eine ArrayList
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER); // aufspliten der Datensätze am Komma
                reviews.add(Arrays.asList(values));
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex2) {
        }

        Transaction tx = null;
        Session session = null;
        ArrayList<Bewertung> bewertungen = new ArrayList<>();
        // Wir initilizen i vor dem Loop, damit wir in dem Error sehen, in welcher Zeile der Fehler ist.
        int i = 1;

        Bewertung currentBewertung = new Bewertung();

        for (i = 1; i < reviews.size(); i++) {
            try {

                session = sessionFactory.openSession();
                tx = session.beginTransaction();

                //Ausgabe in welcher column der Fehler auftritt
                if (getColumnRowWithoutQuotation(i,0, reviews).isEmpty()) {
                    throw new NullValueException("Produkt Nr");
                }
                else if(getColumnRowWithoutQuotation(i,1, reviews).isEmpty()){
                    throw new NullValueException("Rating");

                } else if(getColumnRowWithoutQuotation(i,6, reviews).isEmpty()) {
                    throw new NullValueException("Content");
                }
                String username = getColumnRowWithoutQuotation(i,4, reviews);
                currentBewertung.setUsername(username);

                Produkt produkt = session.get(Produkt.class, getColumnRowWithoutQuotation(i,0, reviews));
                currentBewertung.setProdukt(produkt);
                currentBewertung.setPunkte(Integer.parseInt(getColumnRowWithoutQuotation(i,1, reviews)));
                currentBewertung.setRezension(reviews.get(i).get(6).substring(0,Math.min(reviews.get(i).get(6).length(), 200)).replace("\"", ""));
                //commit der derzeit geladenen Bewertung
                bewertungen.add(currentBewertung);
                session.save(currentBewertung);
                currentBewertung.updateProduktRating(session);
                tx.commit();

            } catch (NullValueException e) {
                // Collecten der Fehlermeldungen und schreiben in die Errorlist
                if(tx != null) tx.rollback();
                System.err.println("Null Fehler beim Laden von Review " + i);
                e.printStackTrace();
                nullFehlerCount++;
                errorList.append("Null Fehler beim Laden von Review ").append(i);
                errorList.append(e);

                errorList.append("\n");
            }catch (PersistenceException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
                konsistenzErrorCount++;
                errorList.append("Konsistenz fehler bei Review ").append(i);
                errorList.append(Arrays.toString(e.getStackTrace()));

                errorList.append("\n");
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                session.close();
            }
        }
        
        File errorFile =  new File(file+"error.log");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(errorFile))) {
            // Ausgabe der Fehler in die reviews.csverror.los Datei
            writer.append(errorList);
            writer.append("Konsistenz Errors: ");
            writer.append(Integer.toString( konsistenzErrorCount));
            writer.append("\n");
            writer.append("Null Errors: ");
            writer.append(Integer.toString(nullFehlerCount));
        }catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
