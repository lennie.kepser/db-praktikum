
package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class Buch {


    @Id
    private String produktNummer;

    @OneToOne
    @MapsId
    private Produkt produkt;
    @Column(nullable = true)
    private int seitenZahl;
    @Column(nullable = false)
    private String erscheinungsDatum;
    @Column(nullable = false)
    private String ISBN;
    @Column(nullable = false)
    private String Verlag;

    public String getVerlag() {
        return Verlag;
    }

    public void setVerlag(String Verlag) {
        this.Verlag = Verlag;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getISBN() {
        return ISBN;
    }

    public String getErscheinungsDatum() {
        return erscheinungsDatum;
    }

    public void setErscheinungsDatum(String erscheinungsDatum) {
        this.erscheinungsDatum = erscheinungsDatum;
    }

    public int getSeitenZahl() {
        return seitenZahl;
    }

    public void setSeitenZahl(int seitenZahl) {
        this.seitenZahl = seitenZahl;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProduktNummer(Produkt produktNummer) {
        this.produkt = produktNummer;
    }
}
