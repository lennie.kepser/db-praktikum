package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class DVD {
    @Id
    private String produktNummer;

    @OneToOne
    @MapsId
    private Produkt produkt;
    @Column(nullable = false)
    private int laufZeit;
    @Column(nullable = false)
    private String regionCode;
    @Column(nullable = false)
    private String format;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public int getLaufZeit() {
        return laufZeit;
    }

    public void setLaufZeit(int laufZeit) {
        this.laufZeit = laufZeit;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }
}
