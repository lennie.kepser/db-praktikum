package com.example.demo;

import org.hibernate.*;

import javax.persistence.PersistenceException;
import javax.xml.XMLConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class ShopLoader {

    private String getChildValue(Element elem, String childName,boolean nullable) throws NullValueException{
        //Rückgabe der Childentity bzw. Überprüfung, ob Childelement leer ist
        if (elem.getElementsByTagName(childName).getLength() == 0){
            if (nullable){
                return "";
            }else{
                throw new NullValueException(childName);
            }
        }
        String textContext = elem.getElementsByTagName(childName).item(0).getTextContent();
        if (!nullable && textContext.equals("")){
            throw new NullValueException(childName);
        }
        return textContext;
    }

    private int parseNonOptionalInt(String input, String tagname) throws NullValueException{
        //parsen eines nicht optionalen Strings zu einem Integer
        if (input.isEmpty()){
            throw new NullValueException(tagname);
        }
        return Integer.parseInt(input);
    }

    private String getNonOptionalAttribute(Element element, String attributeName, String errorName) throws NullValueException{
        //Überprüfen, ob das Attribut nicht leer ist und Rückgabe des Attributwertes
        if (!element.hasAttribute(attributeName) || element.getAttribute(attributeName).isEmpty()){
            throw new NullValueException(attributeName + " ("+errorName+")");
        }
        return element.getAttribute(attributeName);
    }


    // Da die Beteiligten Relations gleich aufgebaut ist, haben wir diese Funktion geschrieben, die alle Implementations von
    // dem Beteiligte interface aus einem parentElement rauslesen kann und in die Datenbank schreibt.
    private <T> void saveBeteiligte(Element parentElement, Session session, SessionFactory sessionFactory, String tagName, Produkt produkt, boolean optional, BeteiligungsArt beteiligungsArt) throws NullValueException{
        if (parentElement.getElementsByTagName(tagName).getLength() == 0){
            if (optional){
                return;
            }else{
                throw new NullValueException(tagName);
            }
        }

        NodeList beteiligteList = parentElement.getElementsByTagName(tagName);
        // Wenn Liste leer ist weiter gehen falls optional, ansonsten invalider Datensatz

        for (int b = 0; b < beteiligteList.getLength(); b++) {
            String name;
            if (((Element) beteiligteList.item(b)).hasAttribute(("name"))){
                name = ((Element) beteiligteList.item(b)).getAttribute("name");
            }else{
                name = beteiligteList.item(b).getTextContent();

            }

            Beteiligter neuerBeteiligter = new Beteiligter();

            neuerBeteiligter.setName(name);
            BeteiligtAn neueBeteiligung = new BeteiligtAn();
            neueBeteiligung.setBeteiligungsart(beteiligungsArt);
            neueBeteiligung.setName(name);
            neueBeteiligung.setProduktNummer(produkt);
            Beteiligter prevBeteiliger = session.get(Beteiligter.class, neuerBeteiligter.getName());
            // Wir prüfen ob der Beteiligte bereits in der Datenbank vorhanden ist. Falls nicht fügen wir ihn hinzu.
            if (prevBeteiliger == null){
                session.save(neuerBeteiligter);

            }
            session.save(neueBeteiligung);



        }
    }

    private void saveTitel(Element parentElement, Session session, Produkt produkt) throws NullValueException{
        if (parentElement.getElementsByTagName("tracks").getLength() == 0){
            throw new NullValueException("tracks");
        }

        NodeList titelListe = parentElement.getElementsByTagName("tracks").item(0).getChildNodes();
        // Wenn Liste leer ist weiter gehen falls optional, ansonsten invalider Datensatz
        if (titelListe.getLength() == 0){
            throw new NullValueException("title");
        }
        int trackCount = 0;
        for (int i = 0; i < titelListe.getLength(); i++) {
            if (titelListe.item(i).getNodeType() != Node.ELEMENT_NODE){
                continue;
            }
            Titel newTitel = new Titel();
            String name = titelListe.item(i).getTextContent();
            if (name.isEmpty()){
                throw new NullValueException("title name");
            }
            newTitel.setName(name);
            newTitel.setProdukt(produkt);
            newTitel.setNummer(trackCount);

            session.save(newTitel);
            trackCount++;
        }
    }


    private void setShop(Session session, Filiale currentShop, Element shop) throws NullValueException, PersistenceException {


        if(shop.getAttribute("name").isEmpty() || shop.getAttribute("zip").isEmpty() || shop.getAttribute("street").isEmpty()) {
            throw new NullValueException("Shop");
        }
        currentShop.setName(shop.getAttribute("name"));
        currentShop.setAnschrift(shop.getAttribute("street") + " " +shop.getAttribute("zip"));
        session.save(currentShop);
     }

     private void loadProduct(Element produktElement, Session session, ArrayList<AehnlichesProdukt> aehnlicheProdukte, SessionFactory sessionFactory, Filiale currentShop) throws NullValueException, PersistenceException{

         Produkt currentProdukt = new Produkt();

         String type = getNonOptionalAttribute(produktElement,"pgroup", "produkttyp");
         String produktID = getNonOptionalAttribute(produktElement,"asin", "produktid");
         String salesrank = produktElement.getAttribute("salesrank");
         currentProdukt.setProduktNummer(produktID);
         if (!salesrank.isEmpty()){
             currentProdukt.setVerkaufsRang(Integer.parseInt(salesrank));
         }
         currentProdukt.setTitel(getChildValue(produktElement,"title", false));


         if (produktElement.getElementsByTagName("details").getLength() != 0){
             Element details = (Element) produktElement.getElementsByTagName("details").item(0);

             if (details.hasAttribute("img")) {
                 currentProdukt.setBild(details.getAttribute("img"));
             }
         }

         if(!produktElement.getAttribute("picture").isEmpty()) {
             currentProdukt.setBild(produktElement.getAttribute("picture"));
         }

         session.save(currentProdukt);



         NodeList similarProducts = produktElement.getElementsByTagName("sim_product");
         for (int b = 0; b < similarProducts.getLength(); b++){
             // Wir fügen die similarProduct Relation in beide Richtungen ein, da die Beziehung Symmetrisch ist.
             AehnlichesProdukt newAehnliches = new AehnlichesProdukt();
             String secondID = ((Element) similarProducts.item(b)).getElementsByTagName("asin").item(0).getTextContent();
             newAehnliches.setTransientProdukt1(produktID);
             newAehnliches.setTransientProdukt2(secondID);
             aehnlicheProdukte.add(newAehnliches);
         }

         if (type.equals("Music")) {
             MusikCD currentMusicCD = new MusikCD();
             currentMusicCD.setProdukt(currentProdukt);
             saveBeteiligte(produktElement, session, sessionFactory, "label", currentProdukt, true, BeteiligungsArt.LABEL);
             saveBeteiligte(produktElement, session, sessionFactory,"artist", currentProdukt, true, BeteiligungsArt.KUENSTLER);
             saveTitel(produktElement, session, currentProdukt);
             Element musicSpecs = (Element) produktElement.getElementsByTagName("musicspec").item(0);
             currentMusicCD.setErscheinungsDatum(getChildValue(musicSpecs, "releasedate", true));

             session.save(currentMusicCD);
         }

         if (type.equals("Book")) {
             Buch currentBuch = new Buch();
             currentBuch.setProduktNummer(currentProdukt);
             Element bookSpecs = (Element) produktElement.getElementsByTagName("bookspec").item(0);
             String pagesString = getChildValue(bookSpecs, "pages", true);
             if (!pagesString.isEmpty()){
                 currentBuch.setSeitenZahl(Integer.parseInt(pagesString));
             }
             Element isbnElement = (Element) bookSpecs.getElementsByTagName("isbn").item(0);
             currentBuch.setISBN(getNonOptionalAttribute(isbnElement,"val","isbn"));
             Element releaseDate = (Element) bookSpecs.getElementsByTagName("publication").item(0);
             currentBuch.setErscheinungsDatum(getNonOptionalAttribute(releaseDate,"date","releasedate"));
             saveBeteiligte(produktElement, session, sessionFactory, "author", currentProdukt, true, BeteiligungsArt.AUTOR);

             if (produktElement.getElementsByTagName("publishers").item(0).getChildNodes().getLength() == 0) {
                 throw new NullValueException("Book");
             }
             currentBuch.setVerlag(produktElement.getElementsByTagName("publishers").item(0).getFirstChild().getTextContent());
             session.save(currentBuch);
         }

         if (type.equals("DVD")){
             DVD currentDVD = new DVD();
             Element dvdSpecs = (Element) produktElement.getElementsByTagName("dvdspec").item(0);
             currentDVD.setProdukt(currentProdukt);
             currentDVD.setRegionCode(getChildValue(dvdSpecs, "regioncode", true));
             currentDVD.setFormat(getChildValue(dvdSpecs,"format", true));
             String runningTime = getChildValue(dvdSpecs, "runningtime", true);
             if (!runningTime.equals("")){
                 currentDVD.setLaufZeit(Integer.parseInt(runningTime));
             }
             saveBeteiligte(produktElement, session, sessionFactory,"actor", currentProdukt, true, BeteiligungsArt.ACTOR);
             saveBeteiligte(produktElement, session, sessionFactory, "creator", currentProdukt, true, BeteiligungsArt.CREATOR);
             saveBeteiligte(produktElement, session, sessionFactory,"director", currentProdukt, true, BeteiligungsArt.DIRECTOR);

             session.save(currentDVD);
         }


     }

     private void loadBietetAn(Session session, Element produktElement, String produktID, Filiale currentShop){
         Element preisElement = (Element) produktElement.getElementsByTagName("price").item(0);
         BietetAn bietetAn = new BietetAn();
         Produkt produkt = session.get(Produkt.class, produktID);
         bietetAn.setProduktNummer(produkt);
         bietetAn.setName(currentShop);
         bietetAn.setZustand(preisElement.getAttribute("state"));

         if (preisElement.getTextContent().isEmpty()){
             bietetAn.setVerfuegbarkeit(false);
         } else {
             float preis = Float.parseFloat(preisElement.getTextContent());
             if (preis>0){
                 bietetAn.setPreis(preis);
             }
             bietetAn.setVerfuegbarkeit(true);
         }

         session.save(bietetAn);
     }

     private void loadAehnlichesProdukt(Session session, String produktID, AehnlichesProdukt LR) throws PersistenceException{

        AehnlichesProdukt RL = new AehnlichesProdukt();
        Produkt produkt2 = session.get(Produkt.class, produktID);
        Produkt produkt1 = session.get(Produkt.class, LR.getTransientProdukt2());
        RL.setProdukt2(produkt1);
        RL.setProdukt1(produkt2);
        LR.setProdukt2(produkt2);
        LR.setProdukt1(produkt1);
        Object prevSimilarProductLR = session.get(AehnlichesProdukt.class, RL);
        if (prevSimilarProductLR == null){

            session.save(LR);
            session.save(RL);

        }
    }

    public boolean loadShop(String file, SessionFactory sessionFactory) {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {

            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            // sichere Verarbeitung der XML Anfragen
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(file));
            doc.getDocumentElement().normalize();
            Element shop = (Element) doc.getElementsByTagName("shop").item(0);

            Filiale currentShop = new Filiale();

            NodeList itemList = shop.getChildNodes();

            Session session = sessionFactory.openSession();
            Session finalSession = session;
            ErrorLogger.tryCatchBlockWrapper(
                    ()->setShop(finalSession, currentShop, shop), file, session, shop.getAttribute("name")
            );

            ArrayList<AehnlichesProdukt> aehnlicheProdukte = new ArrayList<>();
            for (int i = 0; i < itemList.getLength(); i++) {
                Node currentNode = itemList.item(i);


                if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element produktElement = (Element) currentNode;

                    String produktID = produktElement.getAttribute("asin");
                    session = sessionFactory.openSession();
                    Session finalSession1 = session;
                    ErrorLogger.tryCatchBlockWrapper(()->loadProduct(produktElement, finalSession1, aehnlicheProdukte, sessionFactory, currentShop),file,finalSession1,produktID);
                    session = sessionFactory.openSession();
                    Session finalSession2 = session;

                    ErrorLogger.tryCatchBlockWrapper(()->loadBietetAn(finalSession2, produktElement, produktID, currentShop),file, session, "Angebot"+produktID);

                }
            }
            for (AehnlichesProdukt LR : aehnlicheProdukte ){
                if (LR.getTransientProdukt1()==null){
                    continue;
                }
                String produktID = LR.getTransientProdukt1();
                session = sessionFactory.openSession();

                Session finalSession2 = session;
                ErrorLogger.tryCatchBlockWrapper(()->loadAehnlichesProdukt(finalSession2,produktID, LR), file,session,produktID);

            }


        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
