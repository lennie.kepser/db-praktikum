package com.example.demo;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class BeteiligtAn implements Serializable {
    @Id
    @ManyToOne
    private Produkt produktNummer;
    @Id
    private String name;
    @Id
    @Enumerated(EnumType.STRING)
    @Type(type = "com.example.demo.EnumTypePostgreSql")
    private BeteiligungsArt beteiligungsart;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BeteiligungsArt getBeteiligungsart() {
        return beteiligungsart;
    }

    public void setBeteiligungsart(BeteiligungsArt beteiligungsart) {
        this.beteiligungsart = beteiligungsart;
    }

    public Produkt getProduktNummer() {
        return produktNummer;
    }

    public void setProduktNummer(Produkt produktNummer) {
        this.produktNummer = produktNummer;
    }
}
