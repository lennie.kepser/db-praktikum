package com.example.demo;

import javax.management.Query;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ConsoleApp {
    static PostgresSchnittstelle s1 = new PostgresSchnittstelle();
    static Scanner scanner = new Scanner(System.in);

    public static void start() throws IOException {
        s1.init();

        help();
        boolean running = true;

        while (running) {
            System.out.println("Command: ");
            String input = "";
            while (Objects.equals(input, "")){
                input = scanner.nextLine();
            }
            switch (input) {

                case "finish":
                    System.out.println("finish");
                    scanner.close();
                    running = false;
                    break;
                case "getProduct":
                    System.out.println("getProduct");
                    getProduct();
                    break;
                case "getProducts":
                    System.out.println("getProducts");
                    getMultipleProducts();
                    break;
                case "getCategoryTree":
                    System.out.println("getCategoryTree");
                    getCategoryTree();
                    break;
                case "getProductsByCategoryPath":
                    System.out.println("getProductsByCategoryPath");
                    getProductsByCatergoryPath();
                    break;
                case "getTopProducts":
                    System.out.println("getTopProducts");
                    getTopProducts();
                    break;
                case "getSimilarCheaperProduct":
                    System.out.println("getSimilarCheaperProduct");
                    getSimilarCheaperProduct();
                    break;
                case "addNewReview":
                    System.out.println("addNewReview");
                    addNewReview();
                    break;
                case "getTrolls":
                    System.out.println("getTrolls");
                    getTrolls();
                    break;
                case "getOffers":
                    System.out.println("getOffers");
                    getOffers();
                    break;
                case "help":
                    System.out.println("help");
                    help();
                    break;
                default:
                    System.out.println("Command "+input+" not found! Try help for more Information");
                    break;
            }

        }


    }

    private static void getOffers() {
        System.out.println("ProduktID eingeben:");
        String produktID = readString();
        List<BietetAn> list = s1.getOffers(produktID);
        if (list.size() == 0){
            System.out.println("Keine Angebote gefunden.");
        }
        for (BietetAn bietetAn : list) {
            System.out.println(bietetAn.getName().getName());
            System.out.println(bietetAn.getPreis()/100);
            System.out.println(bietetAn.getZustand());
            System.out.println("---------------------");
        }
    }

    private static void getTrolls() {
        System.out.println("Ratinggrenze eingeben:");
        int rating = readInt();
        List<String> list = s1.getTrolls(rating);
        if (list.size() == 0){
            System.out.println("Keine Trolls gefunden.");
        }
        for (String string : list) {
            System.out.println(string);
        }
    }

    private static void addNewReview() {
        System.out.println("Zu welchem Produkt soll eine neue Review erstellt werden?");
        String produktID = readString();

        System.out.println("Geben Sie eine Rezension zu dem Produkt an:");
        String rezension = readString();
        System.out.println("Wie lautet Ihr Nutzername?");
        String username = readString();
        System.out.println("Wie viele Punkte moechten Sie vergeben?");
        int rating = readInt();
        int errorCode = s1.addNewReview(produktID, rating, rezension, username);
        if (errorCode == 0){
            System.out.println("Review hinzugefügt!");
        } else if (errorCode == 2) {
            System.out.println("Produkt nicht gefunden");
        } else{
            System.out.println("Unbekannter Fehler");
        }
    }

    private static void getSimilarCheaperProduct() {
        System.out.println("ProduktID eingeben:");
        String produktID = readString();
        List<Produkt> list = s1.getSimilarCheaperProdukt(s1.getProduct(produktID));
        if (list == null){
            System.out.println("Produkt nicht gefunden");
            return;
        }
        if (list.size() == 0){
            System.out.println("Keine billigere Produkte gefunden.");
        }
        for (Produkt produkt : list) {
            System.out.println(produkt.getTitel());
        }
    }

    private static void getTopProducts() {
        System.out.println("Wie viele Top Produkte sollen angezeigt werden?");
        int num = readInt();
        List<Produkt> list = s1.getTopProducts(num);
        if (list.size() == 0){
            System.out.println("Keine Top Produkte gefunden.");
        }
        for (Produkt produkt : list) {
            System.out.println(produkt.getTitel() + " "+ produkt.getRating());
        }
    }

    private static void getProductsByCatergoryPath() {
        System.out.println("Kategoriepfad angeben: ");
        System.out.println("Format: Kategorie1,Kategorie2,Kategorie3 ...");
        String pfad = readString();
        List<String> kategorien = Arrays.asList(pfad.split("\\s*,\\s*"));
        System.out.println(kategorien);
        List<Produkt> list = s1.getProductsByCategoryPath(kategorien);
        if (list.size() == 0){
            System.out.println("Keine Produkte gefunden.");
        }
        for (Produkt produkt : list) {
            System.out.println(produkt.getTitel());
        }
    }

    private static void printTree(int depth, KategorieTreeNode currentNode){
        for(int i = 0; i < depth; i++){
            System.out.print("-");
        }
        System.out.print(" "+currentNode.getKategorie().getName());
        System.out.println("");
        for(KategorieTreeNode nextNode: currentNode.getUnterKategorien()){
            printTree(depth + 1, nextNode);
        }
    }

    private static void getCategoryTree() {
        System.out.println("Geben Sie eine KategorieID ein:");
        String kategorieID = readString();
        KategorieTreeNode kategorieTreeNode = s1.getCategoryTree(kategorieID);
        if (kategorieTreeNode == null) {
            System.out.println("Kategorie nicht gefunden.");
        }else{
            printTree(0, kategorieTreeNode);
        }
    }

    private static void getMultipleProducts() {
        System.out.println("String Pattern eingeben:");
        String pattern = readString();
        List<Produkt> list = s1.getProducts(pattern);
        if (list.size() == 0){
            System.out.println("Keine Produkte gefunden.");
        }
        for (Produkt produkt : list) {
            System.out.println(produkt.getTitel());
        }
    }

    public static void getProduct() {
        System.out.println("Produkt ID eingeben:");
        String id = readString();
        Produkt produkt = s1.getProduct(id);
        if (produkt == null){
            System.out.println("Produkt nicht gefunden.");
            return;
        }
        System.out.println(produkt.getTitel() + " " + produkt.getRating() + " " + produkt.getVerkaufsRang());
    }

    public static void help() {
        System.out.println("List of possible commands:\n");
        System.out.println("init -- Datenbankaufruf/Initialisierung.\n");
        System.out.println("finish -- Beenden der Anwendung.\n");
        System.out.println("getProduct -- Detailinformationen ueber ein Produkt mittels ProduktID\n");
        System.out.println("getProducts(String pattern) -- Titelsuche mehrerer Produkte (Wildcard-Zeichen moeglich)\n");
        System.out.println("getCategoryTree -- ermittelt kompletten Kategoriebaum\n");
        System.out.println("getProductsByCategoryPath -- Suche von Produkten durch den Kategorienamen -> durch den Kategoriepfad\n");
        System.out.println("getTopProducts -- Liefert x Produkte mit höchstem Rating\n");
        System.out.println("getSimilarCheaperProduct -- Liefert fuer ProduktID eine Liste mit aehnlichen und billigeren\n");
        System.out.println("addNewReview -- Ansehen und Hinzufuegen von Reviews\n");
        System.out.println("getTrolls -- Liste von Nutzern mit Unterdurchschnittlicher Bewertung.\n");
        System.out.println("getOffers -- Alle Angebote für ein Produkt(ID)\n");
    }

    public static int readInt() {
        int num;
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("Falsches Format!");
                scanner.next();
            }
            num = scanner.nextInt();
        } while (num <= 0);
        return num;
    }

    public static String readString() {

        String string;
        do {
            while (!scanner.hasNext()) {
                System.out.println("Falsches Format!");
                scanner.next();
            }
            string = scanner.nextLine();
        } while (string == null);
        return string;
    }



}