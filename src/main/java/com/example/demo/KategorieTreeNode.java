package com.example.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class KategorieTreeNode {
    private final Kategorie kategorie;

    private KategorieTreeNode oberKategorie;

    private final List<KategorieTreeNode> unterKategorien;

    public KategorieTreeNode(Kategorie kategorie, Session session){
        this.kategorie = kategorie;
        this.unterKategorien = new ArrayList<>();
        buildFullTree(session);
    }
    public KategorieTreeNode(Kategorie kategorie, KategorieTreeNode oberKategorie, Session session){
        this.kategorie = kategorie;
        this.oberKategorie = oberKategorie;
        this.unterKategorien = new ArrayList<>();
        buildLowerTree(session);
    }
    public KategorieTreeNode(Kategorie kategorie, List<KategorieTreeNode> unterKategorien, Session session){
        this.kategorie = kategorie;
        this.unterKategorien = unterKategorien;
        buildFullTree(session);
    }


    private void buildUpperTree(Session session){
        Kategorie oberKategorie = kategorie.getUeberKategorie();
        if (oberKategorie != null){
            List<KategorieTreeNode> newUnterKategorien = new ArrayList<KategorieTreeNode>();
            newUnterKategorien.add(this);
            this.oberKategorie = new KategorieTreeNode(oberKategorie, newUnterKategorien, session);
        }
    }

    @SuppressWarnings("unchecked")
    private void buildLowerTree(Session session){
        Query<Kategorie> query= session.
                createQuery("from Kategorie where ueberKategorie=:kategorieID");
        query.setParameter("kategorieID", this.kategorie);
        List<Kategorie> unterKategorien = (List<Kategorie>) query.list();

        for (Kategorie uk: unterKategorien) {
            boolean stop = false;
            for (KategorieTreeNode tn: this.unterKategorien){
                if (tn.isEqualTo(uk)){
                    stop = true;
                }
            }
            if (stop){
                continue;
            }
            this.unterKategorien.add(new KategorieTreeNode(
                    uk, this, session
            ));
        }

    }

    public List<KategorieTreeNode> getUnterKategorien(){
        return this.unterKategorien;
    }

    public Kategorie getKategorie(){
        return kategorie;
    }

    public boolean isEqualTo(Kategorie kat){
        return this.kategorie.getKategorieID().equals(kat.getKategorieID());
    }

    private void buildFullTree(Session session){
        buildUpperTree(session);
        buildLowerTree(session);
    }

    public KategorieTreeNode getOberKategorie() {
        return oberKategorie;
    }
}
