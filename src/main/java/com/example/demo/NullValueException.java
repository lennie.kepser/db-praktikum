package com.example.demo;

public class NullValueException extends Exception{
    public NullValueException(String attributeName){
        super("Null value in "+attributeName);
    }


}
