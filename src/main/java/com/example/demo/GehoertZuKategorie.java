package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class GehoertZuKategorie implements Serializable {


    @Id
    @ManyToOne
    @JoinColumn
    private Produkt produkt;


    @Id
    @ManyToOne
    @JoinColumn
    private Kategorie kategorie;

    @Transient
    private String internalProduktNummer;

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }

    public String getInternalProduktNummer() {
        return internalProduktNummer;
    }

    public void setInternalProduktNummer(String internalProduktNummer) {
        this.internalProduktNummer = internalProduktNummer;
    }

    public Kategorie getKategorie() {
        return kategorie;
    }

    public void setKategorie(Kategorie kategorie) {
        this.kategorie = kategorie;
    }
}
