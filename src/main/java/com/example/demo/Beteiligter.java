package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class Beteiligter {
    @Id
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
