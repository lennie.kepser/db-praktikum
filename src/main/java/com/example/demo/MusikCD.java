package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class MusikCD {

    @Id
    private String produktNummer;

    @OneToOne
    @MapsId
    private Produkt produkt;

    @Column(nullable = false)
    private String erscheinungsDatum;

    public String getErscheinungsDatum() {
        return erscheinungsDatum;
    }

    public void setErscheinungsDatum(String erscheinungsDatum) {
        this.erscheinungsDatum = erscheinungsDatum;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }
}
