package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class Produkt {
    @Id
    @Column(length = 50)
    private String produktNummer;

    @Column(nullable = false)
    private String titel;

    @Column(columnDefinition = "int default 0")
    private int amnt_ratings;
    private float rating;

    @Column(nullable = true)
    private int verkaufsRang;

    public String getBild() {
        return bild;
    }

    public void setBild(String bild) {
        this.bild = bild;
    }

    private String bild;

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getRating() {
        return rating;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public int getVerkaufsRang() {
        return verkaufsRang;
    }

    public void setVerkaufsRang(int verkaufsRang) {
        this.verkaufsRang = verkaufsRang;
    }

    public String getProduktNummer() {
        return produktNummer;
    }

    public void setProduktNummer(String produktNummer) {
        this.produktNummer = produktNummer;
    }

    public int getAmnt_ratings() {
        return amnt_ratings;
    }

    public void setAmnt_ratings(int amnt_ratings) {
        this.amnt_ratings = amnt_ratings;
    }
}
