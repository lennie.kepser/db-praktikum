package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class AehnlichesProdukt implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn
    private Produkt produkt1;

    @Id
    @ManyToOne
    @JoinColumn
    private Produkt produkt2;

    @Transient
    private String transientProdukt2;
    @Transient
    private String transientProdukt1;

    public Produkt getProdukt1() {
        return produkt1;
    }

    public void setProdukt1(Produkt produkt1) {
        this.produkt1 = produkt1;
    }



    public Produkt getProdukt2() {
        return produkt2;
    }

    public void setProdukt2(Produkt produkt2) {
        this.produkt2 = produkt2;
    }

    public String getTransientProdukt2() {
        return transientProdukt2;
    }

    public void setTransientProdukt2(String transientProdukt2) {
        this.transientProdukt2 = transientProdukt2;
    }

    public String getTransientProdukt1() {
        return transientProdukt1;
    }

    public void setTransientProdukt1(String transientProdukt1) {
        this.transientProdukt1 = transientProdukt1;
    }
}
