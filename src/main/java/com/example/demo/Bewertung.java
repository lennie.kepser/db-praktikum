package com.example.demo;

import org.hibernate.Session;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class Bewertung implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int bewertungsid;

    @ManyToOne
    @JoinColumn(nullable = true)
    private Kunde kunde;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Produkt produkt;

    @Column(nullable = false)
    private int punkte;
    @Column(length = 1000, nullable = false)
    private String rezension;

    private String username;

    public Bewertung(){

    }

    public Bewertung(Kunde kunde, Produkt produkt, int punkte, String rezension, String username, Session session){
        this.kunde = kunde;
        this.produkt = produkt;
        this.punkte = punkte;
        this.rezension = rezension;
        this.username = username;
        updateProduktRating(session);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPunkte() {
        return punkte;
    }

    public void setPunkte(int punkte) {
        this.punkte = punkte;
    }

    public String getRezension() {
        return rezension;
    }

    public void setRezension(String rezension) {
        this.rezension = rezension;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kontonummer) {
        this.kunde = kontonummer;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produktNummer) {
        this.produkt = produktNummer;
    }

    public void updateProduktRating(Session session){
        if (produkt == null){
            return;
        }
        float prev_rating = produkt.getRating();
        int amnt_ratings = produkt.getAmnt_ratings();
        float new_rating = (amnt_ratings * prev_rating + punkte) / (amnt_ratings + 1);
        produkt.setAmnt_ratings(amnt_ratings + 1);
        produkt.setRating(new_rating);
        session.save(produkt);
    }
}
