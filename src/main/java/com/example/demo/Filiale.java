package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "public")
public class Filiale implements Serializable {
    @Id
    private String name;

    private String anschrift;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnschrift() {
        return anschrift;
    }

    public void setAnschrift(String anschrift) {
        this.anschrift = anschrift;
    }
}
