package com.example.demo;

import javax.persistence.*;

@Entity
@Table(schema = "public")
public class Kunde {
    @Id
    @Column(nullable = false)
    private String kontonummer;
    @Column(nullable = false)
    private String lieferAdresse;

    public String getKontonummer() {
        return kontonummer;
    }

    public void setKontonummer(String kontonummer) {
        this.kontonummer = kontonummer;
    }

    public String getLieferAdresse() {
        return lieferAdresse;
    }

    public void setLieferAdresse(String lieferAdresse) {
        this.lieferAdresse = lieferAdresse;
    }
}
