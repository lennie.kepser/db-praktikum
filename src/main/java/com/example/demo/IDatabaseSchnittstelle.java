package com.example.demo;


import java.util.List;

public interface IDatabaseSchnittstelle {
    public void init();

    public void finish();

    public Produkt getProduct(String id);

    public List<Produkt> getProducts(String pattern);

    // Muss neue Klasse returnen
    public KategorieTreeNode getCategoryTree(String kategorieId);

    public List<Produkt> getProductsByCategoryPath(List<String> pfad);

    public List<Produkt> getTopProducts(int k);

    public List<Produkt> getSimilarCheaperProdukt(Produkt produkt);

    // Returns Error Code: 0 = Success, 1 = Persistence Exception
    public int addNewReview( String produktId, int punkte, String rezension, String username);

    public List<String> getTrolls(double avgRatingThreshold);

    public List<BietetAn> getOffers(String produktId);
}
