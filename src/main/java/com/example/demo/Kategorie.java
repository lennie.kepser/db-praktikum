package com.example.demo;



import javax.persistence.*;

@Entity
@Table(schema = "public",
indexes = {
        @Index(columnList = "ueberkategorie_kategorieid")
})
public class Kategorie{

    @Transient
    private Kategorie oberKategorie;

    @Column(nullable = false)
    private String name;

    @Id
    private String kategorieID;

    @ManyToOne
    @JoinColumn(nullable = true)
    private Kategorie ueberKategorie;

    public Kategorie getUeberKategorie() {
        return ueberKategorie;
    }

    public void setUeberKategorie(Kategorie ueberKategorie) {
        this.ueberKategorie = ueberKategorie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKategorieID() {
        return kategorieID;
    }

    public void setKategorieID(String kategorieID) {
        this.kategorieID = kategorieID;
    }

    public Kategorie getOberKategorie() {
        return oberKategorie;
    }

    public void setOberKategorie(Kategorie oberKategorie) {
        this.oberKategorie = oberKategorie;
    }
}
