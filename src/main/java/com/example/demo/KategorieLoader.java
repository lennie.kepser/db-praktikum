package com.example.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class KategorieLoader {

    private List<Kategorie> kategorien;
    private List<GehoertZuKategorie> gehoertZuKategorien;
    //@SuppressWarnings({ "unchecked", "null" })
    public boolean readCategory(String categoryFile) {
        int categoryID = 0;
        kategorien = new ArrayList<Kategorie>();
        gehoertZuKategorien = new ArrayList<GehoertZuKategorie>();
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(categoryFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            Kategorie letzteKategorie = null;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    // If we have an item element, we create a new item
                    String elementName = startElement.getName().getLocalPart();
                    event = eventReader.nextEvent();
                    switch (elementName) {
                        case "category":
                            Kategorie neueKategorie = new Kategorie();
                            kategorien.add(neueKategorie);
                            neueKategorie.setOberKategorie(letzteKategorie);
                            if (letzteKategorie != null){
                                neueKategorie.setUeberKategorie(letzteKategorie);
                            }
                            letzteKategorie = neueKategorie;
                            neueKategorie.setName(event.asCharacters().getData().trim());
                            neueKategorie.setKategorieID(Integer.toString(categoryID));
                            categoryID++;
                            break;

                        case "item":
                            GehoertZuKategorie neueKategorieRelation = new GehoertZuKategorie();
                            neueKategorieRelation.setInternalProduktNummer(event.asCharacters().getData());
                            if (letzteKategorie == null){
                                throw new Exception("Item does not have parent category");
                            }
                            neueKategorieRelation.setKategorie(letzteKategorie);
                            gehoertZuKategorien.add(neueKategorieRelation);
                            break;

                    }
                }

                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("category")) {
                        if (letzteKategorie != null){
                            letzteKategorie = letzteKategorie.getOberKategorie();
                        }

                    }
                }

            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    public void saveInDatabase(SessionFactory sessionFactory){
        int konsistenzErrorCount = 0;
        StringBuilder errorList = new StringBuilder();
        for (Kategorie kategorie : kategorien) {
            Transaction tx = null;
            Session session = sessionFactory.openSession();

            try {
                tx = session.beginTransaction();

                session.save(kategorie);
                tx.commit();
            } catch (PersistenceException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
                konsistenzErrorCount++;
                errorList.append("Konsistenz fehler bei Kategorie: ").append(kategorie.getName());
                errorList.append(Arrays.toString(e.getStackTrace()));

                errorList.append("\n");
            } finally {
                session.close();
            }
        }
        for (GehoertZuKategorie gehoertZuKategorie : gehoertZuKategorien) {
            Transaction tx = null;
            Session session = sessionFactory.openSession();

            try {
                tx = session.beginTransaction();
                Produkt produkt = session.get(Produkt.class, gehoertZuKategorie.getInternalProduktNummer());
                gehoertZuKategorie.setProdukt(produkt);
                session.save(gehoertZuKategorie);
                tx.commit();
            } catch (PersistenceException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
                konsistenzErrorCount++;
                errorList.append("Konsistenz fehler bei gehoertZuKategorie mit ProduktID: ").append(gehoertZuKategorie.getProdukt());
                errorList.append(Arrays.toString(e.getStackTrace()));

                errorList.append("\n");
            } finally {
                session.close();
            }
        }
        File errorFile =  new File("kategorie.error.log");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(errorFile))) {
            writer.append(errorList);
            writer.append("\n");
            writer.append("Konsistenz Errors: ");
            writer.append(Integer.toString(konsistenzErrorCount));
        }catch(IOException e){
            e.printStackTrace();
        }

    }



}
