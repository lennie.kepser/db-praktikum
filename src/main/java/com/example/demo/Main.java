package com.example.demo;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;


import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class Main {

    public static void main(String[] args)  {

        IDatabaseSchnittstelle db = new PostgresSchnittstelle();
        db.init();
        /*System.out.println(db.getProduct("3000147012"));
        System.out.println(db.getProducts("300%"));
        System.out.println(db.getCategoryTree("12"));
        List<String> path = new ArrayList<>();
        path.add("Formate");
        path.add("Box-Sets");
        path.add("Alternative");
        System.out.println(db.getProductsByCategoryPath(path));
        System.out.println(db.getTopProducts(4));
        Produkt pr = db.getProduct("051");
        System.out.println(db.getSimilarCheaperProdukt(pr));
        //db.addNewReview("3000147012", 4, "Great!", "Leonhard");
        System.out.println(db.getTrolls(2));
        System.out.println(db.getOffers("3000147012"));
*/

        try {
            ConsoleApp.start();
            db.finish();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}