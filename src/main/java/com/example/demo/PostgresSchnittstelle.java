package com.example.demo;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.result.Output;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PostgresSchnittstelle implements IDatabaseSchnittstelle {
    private SessionFactory sessionFactory;

    private String errorLogFile = "error.log";

    public void createSessionFactory(boolean recreateDatabase) {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            if (recreateDatabase){
                MetadataSources metadataSources = new MetadataSources(registry);

                Metadata metadata = metadataSources.buildMetadata();

                SchemaExport schemaExport = new SchemaExport();
                schemaExport.setFormat(true);
                schemaExport.setOutputFile("create.sql");

                schemaExport.setHaltOnError(true);
                try {
                    schemaExport.execute(EnumSet.of(TargetType.DATABASE), SchemaExport.Action.BOTH, metadata, registry);
                }catch(Exception e){
                    schemaExport.execute(EnumSet.of(TargetType.DATABASE), SchemaExport.Action.CREATE, metadata, registry);
                }
                loadFiles();
            }
        }
        catch (Exception e) {
            System.out.println(e);

            StandardServiceRegistryBuilder.destroy( registry );
            throw e;

        }
    }


    private void loadFiles(){
        KategorieLoader kategorieLoader = new KategorieLoader();

        ShopLoader shopLoader = new ShopLoader();
        ReviewLoader reviewLoader = new ReviewLoader();

        shopLoader.loadShop("dresden.xml", sessionFactory);
        shopLoader.loadShop("leipzig_transformed.xml", sessionFactory);
        reviewLoader.loadReview("reviews.csv", sessionFactory);
        kategorieLoader.readCategory("categories.xml");
        kategorieLoader.saveInDatabase(sessionFactory);
        ErrorLogger.writeErrorLogs();
    }


    public void init(){
        createSessionFactory(false);
    }

    public void finish(){
        closeFactory();
    }

    public Produkt getProduct(String id){

        Session session = sessionFactory.openSession();
        Produkt produkt = session.get(Produkt.class, id);
        session.close();
        return produkt;
    }

    public List<Produkt> getProducts(String pattern){
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb=session.getCriteriaBuilder();
        CriteriaQuery<Produkt> cquery = cb.createQuery(Produkt.class);
        Root<Produkt> root = cquery.from(Produkt.class);
        cquery.select(root);
        Predicate predicate=cb.like(root.get("produktNummer"), pattern);
        cquery.where(predicate);
        Query<Produkt> q = session.createQuery(cquery);
        List<Produkt> output = q.list();
        session.close();
        return output;
    }

    // Muss neue Klasse returnen
    public KategorieTreeNode getCategoryTree(String kategorieId){
        Session session = sessionFactory.openSession();
        Kategorie kategorie = session.get(Kategorie.class, kategorieId);
        if (kategorie == null){
            return null;
        }
        KategorieTreeNode output = new KategorieTreeNode(kategorie, session);
        while (output.getOberKategorie()!=null){
            output = output.getOberKategorie();
        }
        session.close();
        return  output;
    }

    @SuppressWarnings("unchecked")
    public List<Kategorie> getCategoriesFromName(String kategorieName){
        Session session = sessionFactory.openSession();

        Query<Kategorie> query= session.
                createQuery("from Kategorie where name=:kategorieName");
        query.setParameter("kategorieName", kategorieName);
        List<Kategorie> output = (List<Kategorie>) query.list();

        session.close();
        return  output;
    }

    @SuppressWarnings("unchecked")
    public List<Produkt> getProductsByCategoryPath(List<String> pfad){
        List<Kategorie> rootKategorien = getCategoriesFromName(pfad.get(0));
        if (rootKategorien.size() != 1){
            return new ArrayList<>();
        }
        Kategorie rootKategorie = getCategoriesFromName(pfad.get(0)).get(0);
        Session session = sessionFactory.openSession();

        KategorieTreeNode currentKategorieTreeNode = new KategorieTreeNode(rootKategorie, session);
        for(int i = 1; i < pfad.size(); i++){
            for (KategorieTreeNode unterKat: currentKategorieTreeNode.getUnterKategorien()) {
                if (unterKat.getKategorie().getName().equals(pfad.get(i))){
                    currentKategorieTreeNode = unterKat;
                    break;
                }
            }
        }

        Query<GehoertZuKategorie> query= session.
                createQuery("from GehoertZuKategorie where kategorie=:kategorie");
        query.setParameter("kategorie", currentKategorieTreeNode.getKategorie());
        List<GehoertZuKategorie> gehoertList = query.list();

        ArrayList<Produkt> output = new ArrayList<>();

        for(GehoertZuKategorie current: gehoertList){
            output.add(current.getProdukt());
        }
        session.close();
        return  output;

    }

    @SuppressWarnings("unchecked")
    public List<Produkt> getTopProducts(int k){
        Session session = sessionFactory.openSession();

        Query<Produkt> query= session.
                createQuery("from Produkt order by rating desc");
        query.setMaxResults(k);
        List<Produkt> output = query.list();

        session.close();
        return output;
    }

    @SuppressWarnings("unchecked")
    public List<Produkt> getSimilarCheaperProdukt(Produkt produkt){
        Session session = sessionFactory.openSession();

        Query<BietetAn> preisQuery = session.
                createQuery("from BietetAn where produktNummer = :produkt order by preis asc");
        preisQuery.setParameter("produkt",produkt);
        if (preisQuery.list().size() == 0){
            return null;
        }
        BietetAn mainAngebot =  preisQuery.list().get(0);



        Query<AehnlichesProdukt> query= session.
                createQuery("from AehnlichesProdukt where produkt1 = :produkt1");
        query.setParameter("produkt1",produkt);
        List<AehnlichesProdukt> aehnlichesProdukts = query.list();

        ArrayList<Produkt> output = new ArrayList<>();

        for(AehnlichesProdukt ap : aehnlichesProdukts){
            Query<BietetAn> query1 = session.createQuery(
                    "from BietetAn where produktNummer = :aehnlichesProdukt and preis < :preis");
            query1.setParameter("aehnlichesProdukt",ap.getProdukt2());
            query1.setParameter("preis", mainAngebot.getPreis());
            for (BietetAn angebot: query1.list()){
                output.add(angebot.getProduktNummer());
            }

        }

        return output;

    }

    // Returns Error Code: 0 = Success, 1 = Persistence Exception, 2 = Produkt nicht gefunden
    public int addNewReview( String produktId, int punkte, String rezension, String username){

        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try {
            Produkt produkt = session.get(Produkt.class, produktId);
            if (produkt == null){
                return 2;
            }
            tx = session.beginTransaction();

            Bewertung newBewertung = new Bewertung(
                    null, produkt, punkte, rezension, username, session
            );
            session.save(newBewertung);
            tx.commit();
        } catch (PersistenceException e) {
            if (tx != null) tx.rollback();

            e.printStackTrace();
            return 1;
        }finally {
            session.close();
        }
        return 0;
    }

    @SuppressWarnings("unchecked")
    public List<String> getTrolls(double avgRatingThreshold){

        Session session = sessionFactory.openSession();

        Query<String> query= session.
                createQuery(
                        "select bewertung.username from Bewertung bewertung group by username having avg(bewertung.punkte) < :threshold");
        query.setParameter("threshold", avgRatingThreshold);

        List<String> output = query.list();

        session.close();
        return output;
    }

    @SuppressWarnings("unchecked")
    public List<BietetAn> getOffers(String produktId){
        Produkt produkt = getProduct(produktId);
        Session session = sessionFactory.openSession();
        Query<BietetAn> query = session.createQuery(
                "from BietetAn where produktNummer = :produkt"
        );
        query.setParameter("produkt", produkt);
        List<BietetAn> output = query.list();
        session.close();
        return output;
    }

    public void closeFactory() {
        if (sessionFactory != null) {
            try {
                sessionFactory.close();
            } catch (HibernateException ignored) {
                System.err.println("Couldn't close SessionFactory");
            }
        }
    }
}
