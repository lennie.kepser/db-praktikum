package com.example.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.xml.sax.SAXException;

import javax.persistence.PersistenceException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;

public class ErrorLogger {
    private static final HashMap<String, StringBuilder> errorDict = new HashMap<String, StringBuilder>();
    private static final HashMap<String, Integer> errorCountDictKonst = new HashMap<String, Integer>();
    private static final HashMap<String, Integer> errorCountDictNull = new HashMap<String, Integer>();
    interface VoidCallable {
        void call() throws PersistenceException, NullValueException;
    }


    private static void increaseErrorCount( HashMap<String, Integer> errorCountDict, String key){
        Integer oldValue = errorCountDict.get(key);
        errorCountDict.put(key, oldValue + 1);
    }

    public static void tryCatchBlockWrapper(VoidCallable func, String errorLogName, Session session,  String id){
        if (!errorDict.containsKey(errorLogName)){
            errorDict.put(errorLogName, new StringBuilder());
            errorCountDictKonst.put(errorLogName, 0);
            errorCountDictNull.put(errorLogName, 0);
        }
        StringBuilder errorList = errorDict.get(errorLogName);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            func.call();
            tx.commit();
        }catch (PersistenceException e){
            // Konsistenz Fehler sind fast ausschließlich doppelte Produkte. Diese werden nicht gespeichert und ins Error Log geschrieben
            if (tx != null) tx.rollback();
            errorList.append("Konsistenz fehler bei ProduktID: ");
            if (id != null) {
                System.err.println("Konsistenz Fehler beim laden von Produkt " + id);
                errorList.append(id);
            }
            e.printStackTrace();
            increaseErrorCount(errorCountDictKonst, errorLogName);
            errorList.append(Arrays.toString(e.getStackTrace()));

            errorList.append("\n");
        }
        catch (NullValueException e) {
            // Bei Null values werden diese ins Log geschrieben und die transaktion abgebrochen
            if (tx != null) tx.rollback();
            errorList.append(e.toString());

            if (id != null) {
                System.err.println("Error beim laden von Produkt " + id);
                errorList.append(" ProduktID: ").append(id);

            }
            e.printStackTrace();
            increaseErrorCount(errorCountDictNull, errorLogName);

            errorList.append("\n");
        } finally {
            session.close();

        }
    }
    public static void writeErrorLogs(){
        for (String key : errorDict.keySet()){
            StringBuilder errorList = errorDict.get(key);
            File errorFile =  new File(key+"error.log");

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(errorFile))) {
                writer.append(errorList);
                writer.append("\n");
                writer.append("Konsistenz Errors: ");
                writer.append(Integer.toString(errorCountDictKonst.get(key)));
                writer.append("\n");
                writer.append("Null Errors: ");
                writer.append(Integer.toString(errorCountDictNull.get(key)));
            }catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
